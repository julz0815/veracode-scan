const { getVeracodeApplicationForPolicyScan, getVeracodeSandboxIDFromProfile, createSandboxRequest, getVeracodeApplicationScanStatus, getVeracodeApplicationFindings
} = require('./services/application-service.js');
const { downloadJar } = require('./api/java-wrapper.js');
const { createSandboxBuild, createBuild, uploadFile, beginPreScan, checkPrescanSuccess, getModules, beginScan, checkScanSuccess
} = require('./services/scan-service.js');
const appConfig = require('./app-cofig.js');
const argv = require('minimist')(process.argv.slice(2));

const vid = argv.vid;
const vkey = argv.vkey;
const appname = argv.appname;
const version = argv.version;
const filepath = argv.filepath;
const createprofile = argv.createprofile;
const include = argv.include || '';
const policy = argv.policy || '';
const teams = argv.teams || '';
const scantimeout = argv.scantimeout || '';
const deleteincompletescan = argv.deleteincompletescan || '';
const failbuild = argv.failbuild || '';
const createsandbox = argv.createsandbox || '';
const sandboxname = argv.sandboxname || '';

const POLICY_EVALUATION_FAILED = 9;
const SCAN_TIME_OUT = 8;

function checkParameters() {
  if (!vid || !vkey || !appname || !version || !filepath) {
    console.log('vid, vkey, appname, version, and filepath are required');
    return false;
  }
  if (vid === '' || vkey === '' || appname === '' || version == '' || filepath === '') {
    console.log('vid, vkey, appname, version, and filepath are required');
    return false;
  }
  if (createprofile !== 'true' && createprofile !== 'false') {
    console.log('createprofile must be set to true or false');
    return false;
  }
  if (isNaN(scantimeout)) {
    console.log('scantimeout must be a number');
    return false;
  }
  if (failbuild.toLowerCase() !== 'true' && failbuild.toLowerCase() !== 'false') {appConfig(vid) 
    console.log('failbuild must be set to true or false');
    return false;
  }
  if (deleteincompletescan.toLowerCase() !== 'true' && deleteincompletescan.toLowerCase() !== 'false') {
    console.log('deleteincompletescan must be set to true or false');
    return false;
  }
  return true;
}

async function run() {
  let responseCode = 0;

  if (!checkParameters())
    return;

  console.log(`Getting Veracode Application for Policy Scan: ${appname}`)
  const veracodeApp = await getVeracodeApplicationForPolicyScan(vid, vkey, appname, policy, teams, createprofile);
  if (veracodeApp.appId === -1) {
    console.log(`Veracode application profile Not Found. Please create a profile on Veracode Platform, \
      or set "createprofile" to "true" in the pipeline configuration to automatically create profile.`);
    return;
  }
  console.log(`Veracode App Id: ${veracodeApp.appId}`);
  console.log(`Veracode App Guid: ${veracodeApp.appGuid}`);

  const jarName = await downloadJar();

  let buildId;
  let sandboxID;
  let sandboxGUID;
  const mylaunchDate = new Date();
  try {
    if (sandboxname !== ''){
      console.log(`Running a Sandbox Scan: '${sandboxname}' on applicaiton: '${appname}'`);
      const sandboxes = await getVeracodeSandboxIDFromProfile(vid, vkey, veracodeApp.appGuid);

      console.log('Finding Sandbox ID & GUID');
      if (sandboxes.page.total_elements !== 0) {
        for (let i = 0; i < sandboxes._embedded.sandboxes.length; i++){
          if (sandboxes._embedded.sandboxes[i].name.toLowerCase() === sandboxname.toLowerCase()){
            sandboxID = sandboxes._embedded.sandboxes[i].id;
            sandboxGUID = sandboxes._embedded.sandboxes[i].guid
          }
          else {
            console.log(`Not the sandbox (${sandboxes._embedded.sandboxes[i].name}) we are looking for (${sandboxname})`);
          }
        }
      }
      if ( sandboxID == undefined && createsandbox == 'true'){
        core.debug(`Sandbox Not Found. Creating Sandbox: ${sandboxname}`);
        //create sandbox
        const createSandboxResponse = await createSandboxRequest(vid, vkey, veracodeApp.appGuid, sandboxname);
        console.log(`Veracode Sandbox Created: ${createSandboxResponse.name} / ${createSandboxResponse.guid}`);
        sandboxID = createSandboxResponse.id;
        sandboxGUID = createSandboxResponse.guid;
        buildId = await createSandboxBuild(vid, vkey, jarName, veracodeApp.appId, version, deleteincompletescan, sandboxID);
        console.log(`Veracode Sandbox Scan Created, Build Id: ${buildId}`);
      }
      else if ( sandboxID == undefined && createsandbox == 'false'){
        console.log(`Sandbox Not Found. Please create a sandbox on Veracode Platform, \
        or set "createsandbox" to "true" in the pipeline configuration to automatically create sandbox.`);
        return;
      }
      else{
        console.log(`Sandbox Found: ${sandboxID} - ${sandboxGUID}`);
        buildId = await createSandboxBuild(vid, vkey, jarName, veracodeApp.appId, version, deleteincompletescan, sandboxID);
        console.log(`Veracode Sandbox Scan Created, Build Id: ${buildId}`);
      }
    }
    else{
      console.log(`Running a Policy Scan: ${appname}`);
      buildId = await createBuild(vid, vkey, jarName, veracodeApp.appId, version, deleteincompletescan);  
      console.log(`Veracode Policy Scan Created, Build Id: ${buildId}`);
    }
  } catch (error) {
    console.log('Failed to create Veracode Scan. App not in state where new builds are allowed.');
    return;
  }

  const uploaded = await uploadFile(vid, vkey, jarName, veracodeApp.appId, filepath, sandboxID);
  console.log(`Artifact(s) uploaded: ${uploaded}`);

  // return and exit the app if the duration of the run is more than scantimeout
  let endTime = new Date();
  if (scantimeout !== '') {
    const startTime = new Date();
    endTime = new Date(startTime.getTime() + scantimeout * 1000 * 60);
  }

  console.log(`scantimeout: ${scantimeout}`);
  console.log(`include: ${include}`)
  
  if (include === '' && uploaded > 0) {
    const autoScan = true;
    await beginPreScan(vid, vkey, jarName, veracodeApp.appId, autoScan, sandboxID);
    if (scantimeout === '') {
      console.log('Static Scan Submitted, please check Veracode Platform for results');
      return;
    }
  } 
  else if (uploaded > 0)
  {
    const autoScan = false;
    const prescan = await beginPreScan(vid, vkey, jarName, veracodeApp.appId, autoScan, sandboxID);
    console.log(`Pre-Scan Submitted: ${prescan}`);
    while (true) {
      await sleep(appConfig(vid).pollingInterval);
      console.log('Checking for Pre-Scan Results...');
      if (await checkPrescanSuccess(vid, vkey, jarName, veracodeApp.appId, sandboxID)) {
        console.log('Pre-Scan Success!');
        break;
      }
      if (scantimeout !== '' && endTime < new Date()) {
        if (failbuild.toLowerCase() === 'true')
          console.log(`Veracode Policy Scan Exited: Scan Timeout Exceeded`);
        else
          console.log(`Veracode Policy Scan Exited: Scan Timeout Exceeded`)
        return;
      }
    }

    const moduleIds = await getModules(vid, vkey, jarName, veracodeApp.appId, include, sandboxID);
    console.log(`Modules to Scan: ${moduleIds.toString()}`);
    const scan = await beginScan(vid, vkey, jarName, veracodeApp.appId, moduleIds.toString(), sandboxID);
    console.log(`Scan Submitted: ${scan}`);
  }
  else 
  {
    console.log('No artifacts to upload');
  }

  console.log('Waiting for Scan Results...');
  let moduleSelectionStartTime = new Date();
  let moduleSelectionCount = 0;
  while (true) {
    await sleep(appConfig(vid).pollingInterval);
    console.log('Checking Scan Results...');
    const statusUpdate = await getVeracodeApplicationScanStatus(vid, vkey, veracodeApp, buildId, sandboxID, sandboxGUID, jarName, mylaunchDate);
    console.log(`Scan Status: ${JSON.stringify(statusUpdate)}`);
    if (statusUpdate.status === 'MODULE_SELECTION_REQUIRED' || statusUpdate.status === 'PRE-SCAN_SUCCESS') {
      moduleSelectionCount++;
      if (moduleSelectionCount === 1)
        moduleSelectionStartTime = new Date();
      if (new Date() - moduleSelectionStartTime > appConfig(vid).moduleSelectionTimeout) {
        console.log('Veracode Policy Scan Exited: Module Selection Timeout Exceeded. ' +
          'Please review the scan on Veracode Platform.' + 
          `https://analysiscenter.veracode.com/auth/index.jsp#HomeAppProfile:${veracodeApp.oid}:${veracodeApp.appId}`);
        responseCode = SCAN_TIME_OUT;
        return responseCode;
      }
    }
    if ((statusUpdate.status === 'PUBLISHED' || statusUpdate.status == 'RESULTS_READY') && statusUpdate.scanUpdateDate) {
      const scanDate = new Date(statusUpdate.scanUpdateDate);
      const policyScanDate = new Date(statusUpdate.lastPolicyScanData);
      if (!policyScanDate || scanDate < policyScanDate) {
        if ((statusUpdate.passFail === 'DID_NOT_PASS' || statusUpdate.passFail == 'CONDITIONAL_PASS') && failbuild.toLowerCase() === 'true'){
          console.log('Policy Violation: Veracode Policy Scan Failed');
          responseCode = POLICY_EVALUATION_FAILED;
        }
        else
          console.log(`Policy Evaluation: ${statusUpdate.passFail}`)
        break;
      } else {
        console.log(`Policy Evaluation: ${statusUpdate.passFail}`)
      }
    }
    
    if (endTime < new Date()) {
      console.log(`Veracode Policy Scan Exited: Scan Timeout Exceeded`);
      responseCode = SCAN_TIME_OUT;
      return responseCode;
    }
  }
  await getVeracodeApplicationFindings(vid, vkey, veracodeApp, buildId, sandboxID, sandboxGUID);
  return responseCode;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

run();